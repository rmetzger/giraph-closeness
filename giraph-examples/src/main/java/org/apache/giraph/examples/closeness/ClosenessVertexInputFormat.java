package org.apache.giraph.examples.closeness;

import java.io.IOException;
import java.util.Set;

import org.apache.giraph.edge.DefaultEdge;
import org.apache.giraph.edge.Edge;
import org.apache.giraph.io.formats.TextVertexInputFormat;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import com.google.common.collect.Sets;

/**
 * VertexInputFormat that supports {@link BitfieldClosenessVertex},
 * {@link FMClosenessVertex}, {@link FM64ClosenessVertex} and {@link HLLClosenessVertex}.
 * 
 * This input format is configurable using two parameters:
 * 	- giraph.examples.closeness.nodeSplit
 * 	- giraph.examples.closeness.edgeSplit
 * 
 * The input format expects the following ASCII based input:
 * 
 * node<NodeSplit>edge<edgeSplit>[edge<edgeSplit>]..
 * node<NodeSplit>edge ...
 * 
 * These parameters can be passed as custom arguments 
 * to GiraphRunner like that: -ca giraph.examples.closeness.nodeSplit=;
 * 
 * @author Aljoscha Krettek, Robert Metzger
 * 
 */
public class ClosenessVertexInputFormat<V extends Writable>
    extends TextVertexInputFormat<LongWritable, V, NullWritable> {

  @Override
  public TextVertexReader createVertexReader(InputSplit split,
      TaskAttemptContext context) throws IOException {
    return new ClosenessVertexReader();
  }

  /**
   * VertexReader that supports {@link BitfieldClosenessVertex}. In this case,
   * the edge values are not used. The files should be in the following format:
   * <vertex id>\t<dest vertex ids> where <dest vertex ids> is a comma separated
   * list of numbers
   */
  public class ClosenessVertexReader extends
      TextVertexReaderFromEachLineProcessed<String[]> {

    String edgeSplit = null;
    String nodeSplit = null;
    @Override
    protected String[] preprocessLine(Text line) throws IOException {
    	nodeSplit = getConf().get("giraph.examples.closeness.nodeSplit");
    	if(nodeSplit == null) {
    		nodeSplit = "\t";
    	}
    	if(nodeSplit.equals("space")) {
    		nodeSplit = " ";
    	}
    	return line.toString().split( nodeSplit );
    }

    @Override
    protected LongWritable getId(String[] tokens) throws IOException {
      return new LongWritable(Integer.parseInt(tokens[0]));
    }

    @Override
    protected V getValue(String[] tokens) throws IOException {
      return null;
    }

    @Override
    protected Iterable<Edge<LongWritable, NullWritable>> getEdges(String[] tokens)
        throws IOException {
    	if(edgeSplit == null) {
    		edgeSplit = getConf().get("giraph.examples.closeness.edgeSplit");
    		if(edgeSplit == null) {
    			edgeSplit = ",";
    		}
    		if(edgeSplit.equals("space")) {
    			edgeSplit = " ";
        	}
    		System.err.println("Using edgeSplit '"+edgeSplit+"'");
    	}
      String targetParts[] = null;
      if(nodeSplit.compareTo(edgeSplit) == 0) {
    	  // splits are equal
    	  targetParts = tokens;
    	  tokens[0] = null;
      } else {
    	  targetParts = tokens[1].split( edgeSplit );
      }
      Set< Edge<LongWritable, NullWritable> > edges = Sets.newHashSet();
      for (String targetStr : targetParts) {
    	if(targetStr == null) continue;
        LongWritable targetId = new LongWritable(Integer.parseInt(targetStr));
        DefaultEdge<LongWritable, NullWritable> e = new DefaultEdge<LongWritable, NullWritable>();
        e.setTargetVertexId(targetId);
        e.setValue(NullWritable.get());
        edges.add(e);
      }
      return edges;
    }
  }
}