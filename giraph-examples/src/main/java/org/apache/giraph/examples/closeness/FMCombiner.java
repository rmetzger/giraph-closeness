package org.apache.giraph.examples.closeness;

import org.apache.giraph.combiner.Combiner;
import org.apache.hadoop.io.LongWritable;

/**
 * Combiner for the {@link FMClosenessVertex}
 * 
 * @author Robert Waury
 *
 */
public class FMCombiner extends Combiner<LongWritable, FMCounterWritable> {

	@Override
	public void combine(LongWritable vertexIndex,
			FMCounterWritable originalMessage,
			FMCounterWritable messageToCombine) {
		originalMessage.merge(messageToCombine);
	}

	@Override
	public FMCounterWritable createInitialMessage() {
		return new FMCounterWritable();
	}

}
