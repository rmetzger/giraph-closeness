package org.apache.giraph.examples.closeness;

import org.apache.giraph.combiner.Combiner;
import org.apache.hadoop.io.LongWritable;

/**
 * Combiner for the {@link HLLClosenessVertex}
 * 
 * @author Robert Waury
 *
 */
public class HLLCombiner extends Combiner<LongWritable, HLLCounterWritable> {

	@Override
	public void combine(LongWritable vertexIndex, HLLCounterWritable originalMessage,
			HLLCounterWritable messageToCombine) {
		originalMessage.merge(messageToCombine);
	}

	@Override
	public HLLCounterWritable createInitialMessage() {
		return new HLLCounterWritable();
	}

}
