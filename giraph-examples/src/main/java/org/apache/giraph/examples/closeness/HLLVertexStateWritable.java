package org.apache.giraph.examples.closeness;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * The Writable that is used as Vertex value by {@link HLLClosenessVertex}. This
 * holds the HLL-Sketch (counter) and a hash map of shortest paths. (One could
 * probably also use an expanding array instead of the map but the map is
 * miniscule compared the the FM-Sketch so it does not matter.
 * 
 * @author Robert Waury
 * 
 */
public class HLLVertexStateWritable implements ClosenessVertex64StateWritable {
	
	private HLLCounterWritable counter;
	private OpenLongLongHashMapWritable shortestPaths;
	
	/**
	 * Create a new vertex state with an empty HLL-Sketch, this is required by
	 * Giraph.
	 */
	public HLLVertexStateWritable() {
	  this.counter = new HLLCounterWritable();
	  this.shortestPaths = new OpenLongLongHashMapWritable();
	}

	/**
	 * Return the HLL-Sketch.
	 */
	public HLLCounterWritable getCounter() {
	  return counter;
	}

	/**
	 * Return the shortest paths hash map.
	 */
	public OpenLongLongHashMapWritable getShortestPaths() {
	  return shortestPaths;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		counter.write(out);
		shortestPaths.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		counter.readFields(in);
		shortestPaths.readFields(in);
	}

}
