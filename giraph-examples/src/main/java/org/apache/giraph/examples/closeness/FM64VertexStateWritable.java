package org.apache.giraph.examples.closeness;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * The Writable that is used as Vertex value by {@link FM64ClosenessVertex}. This
 * holds the FM-Sketch (counter) and a hash map of shortest paths. (One could
 * probably also use an expanding array instead of the map but the map is
 * miniscule compared the the FM-Sketch so it does not matter.
 * 
 * @author Robert Waury
 * 
 */
public class FM64VertexStateWritable implements ClosenessVertex64StateWritable {
	
	private FM64CounterWritable counter;
	private OpenLongLongHashMapWritable shortestPaths;

	  /**
	   * Create a new vertex state with a zero-bucket FM-Sketch, this is required by
	   * Giraph.
	   */
	  public FM64VertexStateWritable() {
	    this.counter = new FM64CounterWritable();
	    this.shortestPaths = new OpenLongLongHashMapWritable();
	  }

	  /**
	   * Return the FM-Sketch.
	   */
	  public FM64CounterWritable getCounter() {
	    return counter;
	  }

	  /**
	   * Return the shortest paths hash map.
	   */
	  public OpenLongLongHashMapWritable getShortestPaths() {
	    return shortestPaths;
	  }

	  /**
	   * Return the number of buckets in the FM-Sketch.
	   */
	  public int getNumBuckets() {
	    return counter.getNumBuckets();
	  }

	  @Override
	  public void write(DataOutput out) throws IOException {
	    counter.write(out);
	    shortestPaths.write(out);
	  }

	  @Override
	  public void readFields(DataInput in) throws IOException {
	    counter.readFields(in);
	    shortestPaths.readFields(in);
	  }

}
