/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.giraph.examples.closeness;

import java.io.IOException;
import org.apache.giraph.examples.closeness.FMVertexStateWritable;
import org.apache.giraph.examples.closeness.FMCounterWritable;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;

/**
 * Graph based implementation of the centrality algorithm detailed in
 * "Centralities in Large Networks: Algorithms and Observations"
 * (http://www.cs.cmu.edu/~ukang/papers/CentralitySDM2011.pdf).
 * 
 * The authors used MapReduce but the algorithm is iterative and perfectly
 * suited for a graph based implementation.
 * 
 * This Vertex uses a Flajolet-Martin Sketch to count the number of neighbours
 * as detailed in the aforementioned paper.
 * 
 * The state held at each vertex is a bit more complex, therefore we need a
 * custom Writable, {@link FMVertexStateWritable} that holds the FM-Sketch and a
 * hash map holding the shortest paths as detailed in the paper.
 * 
 * @author Aljoscha Krettek
 * 
 */
public class FMClosenessVertex extends
	BasicComputation<LongWritable, FMVertexStateWritable, NullWritable, FMCounterWritable> {

  @Override
  public void compute(
  		Vertex<LongWritable, FMVertexStateWritable, NullWritable> vertex,
  		Iterable<FMCounterWritable> messages) throws IOException {
	  if(getSuperstep() == 0) {
		  vertex.setValue(new FMVertexStateWritable());
		  vertex.getValue().getCounter().addNode((int) vertex.getId().get());
	  }
	  int seenCountBefore = vertex.getValue().getCounter().getCount();

	  for (FMCounterWritable inCounter : messages) {
		  vertex.getValue().getCounter().merge(inCounter);
	  }

	  int seenCountAfter = vertex.getValue().getCounter().getCount();

	  if ((seenCountBefore != seenCountAfter) || (getSuperstep() == 0)) {
		  this.sendMessageToAllEdges(vertex, vertex.getValue().getCounter());
	  }

	  // determine last iteration for which we set a value,
	  // we need to copy this to all iterations up to this one
	  // because the number of reachable vertices stays the same
	  // when the compute method is not invoked
	  if (getSuperstep() > 0) {
		  int i = (int) getSuperstep() - 1;
	      while (i > 0) {
	        if (vertex.getValue().getShortestPaths().containsKey(i)) {
	          break;
	        }
	        --i;
	      }
	      int numReachable = vertex.getValue().getShortestPaths().get(i);
	      for (; i < getSuperstep(); ++i) {
	    	  vertex.getValue().getShortestPaths().put(i, numReachable);
	      }
	  }
	  // subtract 1 because our own bit is counted as well
	  vertex.getValue().getShortestPaths().put((int) getSuperstep(),
			  vertex.getValue().getCounter().getCount() - 1);
	  vertex.voteToHalt();
  }


}
