package org.apache.giraph.examples.closeness;

import java.io.IOException;

import org.apache.giraph.edge.Edge;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.io.formats.TextVertexOutputFormat;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

public class ConnectedComponentsOutputFormat extends
		TextVertexOutputFormat<LongWritable, LongWritable, NullWritable> {

	public class ConnectedComponentsVertexWriter extends TextVertexWriter {

		@Override
		public void writeVertex(
				Vertex<LongWritable, LongWritable, NullWritable> vertex)
				throws IOException, InterruptedException {
			StringBuilder result = new StringBuilder();
			result.append(vertex.getId().get());
			result.append("\t");
			result.append(vertex.getValue().get());
			result.append("\t");
			for (Edge<LongWritable, NullWritable> edge : vertex.getEdges()) {
				result.append(edge.getTargetVertexId().get());
				result.append(",");
			}
			// remove last comma
			result.deleteCharAt(result.length()-1);
			getRecordWriter().write(new Text(result.toString()), null);
		}

	}

	@Override
	public TextVertexWriter createVertexWriter(TaskAttemptContext context)
			throws IOException, InterruptedException {
		return new ConnectedComponentsVertexWriter();
	}

}
