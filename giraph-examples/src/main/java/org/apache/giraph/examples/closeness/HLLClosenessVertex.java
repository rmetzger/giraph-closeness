package org.apache.giraph.examples.closeness;

import java.io.IOException;

import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;

/**
 * Graph closeness estimation using a HyperLogLog sketch 
 * for cardinality estimation
 * 
 * @author Robert Waury
 *
 */
public class HLLClosenessVertex 
	extends BasicComputation<LongWritable, HLLVertexStateWritable, NullWritable, HLLCounterWritable> {

	@Override
	public void compute(
			Vertex<LongWritable, HLLVertexStateWritable, NullWritable> vertex,
			Iterable<HLLCounterWritable> messages) throws IOException {
		if(getSuperstep() == 0L) {
			vertex.setValue(new HLLVertexStateWritable());
			vertex.getValue().getCounter().addNode(vertex.getId().get());
		}
		long seenCountBefore = vertex.getValue().getCounter().getCount();

		for (HLLCounterWritable inCounter : messages) {
			vertex.getValue().getCounter().merge(inCounter);
		}

		long seenCountAfter = vertex.getValue().getCounter().getCount();

		if ((seenCountBefore != seenCountAfter) || (getSuperstep() == 0L)) {
			this.sendMessageToAllEdges(vertex, vertex.getValue().getCounter());
		}

		// determine last iteration for which we set a value,
		// we need to copy this to all iterations up to this one
		// because the number of reachable vertices stays the same
		// when the compute method is not invoked
		if (getSuperstep() > 0L) {
		  long l = getSuperstep() - 1L;
		  while (l > 0L) {
		  if (vertex.getValue().getShortestPaths().containsKey(l)) {
		    break;
		  }
		  --l;
		}
		long numReachable = vertex.getValue().getShortestPaths().get(l);
		for (; l < getSuperstep(); ++l) {
			vertex.getValue().getShortestPaths().put(l, numReachable);
		}
		}
		// subtract 1 because our own bit is counted as well
		vertex.getValue().getShortestPaths().put(getSuperstep(),
				  vertex.getValue().getCounter().getCount() - 1L);
		vertex.voteToHalt();
		
	}

}
