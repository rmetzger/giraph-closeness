/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.giraph.examples.closeness;

import java.io.IOException;
import org.apache.giraph.examples.closeness.BitfieldCounterWritable;
import org.apache.giraph.examples.closeness.BitfieldVertexStateWritable;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;

/**
 * Graph based implementation of the centrality algorithm detailed in
 * "Centralities in Large Networks: Algorithms and Observations"
 * (http://www.cs.cmu.edu/~ukang/papers/CentralitySDM2011.pdf).
 * 
 * The authors used MapReduce but the algorithm is iterative and perfectly
 * suited for a graph based implementation.
 * 
 * This Vertex uses bit fields to store the neighbour information, this does not
 * work for larger graphs due to the horrendous memory requirements. For larger
 * graphs use {@link FMClosenessVertex} or {@link FM64ClosenessVertex} which uses a 
 * Flajolet-Martin sketch, as also detailed in the aforementioned paper 
 * or {@link HLLClosenessVertex} which uses a HyperLogLog sketch
 * 
 * The state held at each vertex is a bit more complex, therefore we need a
 * custom Writable, {@link BitfieldVertexStateWritable} that holds the bit field
 * and a hash map holding the shortest paths as detailed in the paper.
 * 
 * @author Aljoscha Krettek, Robert Waury
 * 
 */
public class BitfieldClosenessVertex extends
	BasicComputation<LongWritable, BitfieldVertexStateWritable, NullWritable, BitfieldCounterWritable> {

  /**
   * Simply compute the new vertex value by merging the incoming bit fields into
   * our own bit fields. Also update the shortest paths map that is used to
   * determine the centrality measure when the algorithm terminates.
   * 
   * See paper for more information.
   */
  @Override
  public void compute(
  		Vertex<LongWritable, BitfieldVertexStateWritable, NullWritable> vertex,
  		Iterable<BitfieldCounterWritable> messages) throws IOException {
	  if(this.getSuperstep() > 1L) {
		  int oldCount = vertex.getValue().getCounter().getCount();
		  for (BitfieldCounterWritable bitfieldCounterWritable : messages) {
			vertex.getValue().getCounter().merge(bitfieldCounterWritable);
		  }
		  if(oldCount < vertex.getValue().getCounter().getCount()) {
			  this.sendMessageToAllEdges(vertex, vertex.getValue().getCounter());
		  }
		  int i = (int) this.getSuperstep() - 2; // because of dummy superstep
	      while (i > 0) {
	        if (vertex.getValue().getShortestPaths().containsKey(i)) {
	          break;
	        }
	        --i;
	      }
	      int numReachable = vertex.getValue().getShortestPaths().get(i);
	      for (; i < (int)getSuperstep() - 1; ++i) {
	    	  vertex.getValue().getShortestPaths().put((long) i, numReachable);
	      }
	      // subtract 1 because our own bit is counted as well
	      vertex.getValue().getShortestPaths().put(this.getSuperstep() - 1L, vertex.getValue().getCounter().getCount() - 1);
	      vertex.voteToHalt();
	  } else if(this.getSuperstep() == 1L) {
		  int numBits = (int) this.getTotalNumVertices();
		  // prepare counter
		  vertex.setValue(new BitfieldVertexStateWritable(numBits));
		  vertex.getValue().getCounter().addNode(vertex.getId().get());
		  vertex.getValue().getShortestPaths().put(this.getSuperstep()-1, vertex.getValue().getCounter().getCount()-1);
		  this.sendMessageToAllEdges(vertex, vertex.getValue().getCounter());
	  } else {
		  // dummy step to wait for vertex count
	  }
  }

}
