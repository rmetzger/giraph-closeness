package org.apache.giraph.examples.closeness;

import org.apache.giraph.combiner.Combiner;
import org.apache.hadoop.io.LongWritable;

/**
 * Combiner for the {@link FM64ClosenessVertex}
 * 
 * @author Robert Waury
 *
 */
public class FM64Combiner extends Combiner<LongWritable, FM64CounterWritable> {

	@Override
	public void combine(LongWritable vertexIndex,
			FM64CounterWritable originalMessage,
			FM64CounterWritable messageToCombine) {
		originalMessage.merge(messageToCombine);
	}

	@Override
	public FM64CounterWritable createInitialMessage() {
		return new FM64CounterWritable();
	}

}
