package org.apache.giraph.examples.closeness;

import org.apache.hadoop.io.Writable;

/**
 * Very simple interface to enable having one VertexInputFormat for both the HLL 
 * and 64-Bit Flajolet-Martin Sketch Vertex.
 *
 * @author Robert Waury
 * 
 */
public interface ClosenessVertex64StateWritable extends Writable {
  public OpenLongLongHashMapWritable getShortestPaths();
}
