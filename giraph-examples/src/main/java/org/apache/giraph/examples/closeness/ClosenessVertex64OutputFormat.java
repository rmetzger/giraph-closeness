package org.apache.giraph.examples.closeness;

import java.io.IOException;

import org.apache.giraph.graph.Vertex;
import org.apache.giraph.io.formats.TextVertexOutputFormat;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.mahout.math.list.LongArrayList;

/**
 * VertexOutputFormat that writes the closeness value determined from
 * the shortest paths stored in a ClosenessVertexStateWritable map.
 */
public class ClosenessVertex64OutputFormat<V extends ClosenessVertex64StateWritable>
extends TextVertexOutputFormat<LongWritable, V, NullWritable> {

  public class ClosenessVertexWriter extends TextVertexWriter {

    /**
     * Compute the closeness value as detailed in the paper.
     */
    @Override
    public void writeVertex(Vertex<LongWritable, V, NullWritable> vertex)
        throws IOException, InterruptedException {
      StringBuilder result = new StringBuilder();
      result.append(vertex.getId().get());
      result.append("\t");
      OpenLongLongHashMapWritable shortestPaths = vertex.getValue().getShortestPaths();
      long numVerticesReachable = 0L;
      long sumLengths = 0L;
      for (long key : shortestPaths.keys().elements()) {
        if (key < 1L) {
          continue;
        }
        long newlyReachable = shortestPaths.get(key)
            - shortestPaths.get(key - 1L);
        sumLengths += key * newlyReachable;
        if (shortestPaths.get(key) > numVerticesReachable) {
          numVerticesReachable = shortestPaths.get(key);
        }
      }

      double closeness = 0.0;
      if (numVerticesReachable > 0L) {
        closeness = (double) sumLengths / (double) numVerticesReachable;
      }

      result.append(closeness);
      result.append("\t");

      LongArrayList keys = shortestPaths.keys();
      keys.sort();
      for (long key : keys.elements()) {
        result.append(key);
        result.append(":");
        result.append(shortestPaths.get(key));
        result.append(" ");
      }

      getRecordWriter().write(new Text(result.toString()), null);
    }

  }

  @Override
  public TextVertexWriter createVertexWriter(
		TaskAttemptContext context) throws IOException, InterruptedException {
	return new ClosenessVertexWriter();
  }
}
