package org.apache.giraph.examples.closeness;

import java.io.IOException;

import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;

/**
 * Graph based implementation of the centrality algorithm detailed in
 * "Centralities in Large Networks: Algorithms and Observations"
 * (http://www.cs.cmu.edu/~ukang/papers/CentralitySDM2011.pdf).
 * 
 * The authors used MapReduce but the algorithm is iterative and perfectly
 * suited for a graph based implementation.
 * 
 * This Vertex uses a 64 Bit Flajolet-Martin Sketch to count the number of neighbours
 * as detailed in the aforementioned paper.
 * 
 * The state held at each vertex is a bit more complex, therefore we need a
 * custom Writable, {@link FM64VertexStateWritable} that holds the FM-Sketch and a
 * hash map holding the shortest paths as detailed in the paper.
 * 
 * @author Robert Waury
 * 
 */
public class FM64ClosenessVertex 
	extends BasicComputation<LongWritable, FM64VertexStateWritable, NullWritable, FM64CounterWritable>{

	@Override
	public void compute (
			Vertex<LongWritable, FM64VertexStateWritable, NullWritable> vertex,
			Iterable<FM64CounterWritable> messages) throws IOException {
		if(getSuperstep() == 0L) {
			vertex.setValue(new FM64VertexStateWritable());
			vertex.getValue().getCounter().addNode(vertex.getId().get());
		}
		long seenCountBefore = vertex.getValue().getCounter().getCount();

		for (FM64CounterWritable inCounter : messages) {
			vertex.getValue().getCounter().merge(inCounter);
		}

		long seenCountAfter = vertex.getValue().getCounter().getCount();

		if ((seenCountBefore != seenCountAfter) || (getSuperstep() == 0L)) {
			this.sendMessageToAllEdges(vertex, vertex.getValue().getCounter());
		}

		// determine last iteration for which we set a value,
		// we need to copy this to all iterations up to this one
		// because the number of reachable vertices stays the same
		// when the compute method is not invoked
		if (getSuperstep() > 0L) {
		  long l = getSuperstep() - 1L;
		  while (l > 0L) {
		  if (vertex.getValue().getShortestPaths().containsKey(l)) {
		    break;
		  }
		  --l;
		}
		long numReachable = vertex.getValue().getShortestPaths().get(l);
		for (; l < getSuperstep(); ++l) {
			vertex.getValue().getShortestPaths().put(l, numReachable);
		}
		}
		// subtract 1 because our own bit is counted as well
		vertex.getValue().getShortestPaths().put(getSuperstep(),
				  vertex.getValue().getCounter().getCount() - 1L);
		vertex.voteToHalt();
	}

}
