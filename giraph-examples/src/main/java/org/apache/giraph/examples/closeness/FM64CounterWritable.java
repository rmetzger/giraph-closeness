package org.apache.giraph.examples.closeness;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.io.Writable;

import com.google.common.base.Preconditions;

/**
 * A counter for counting unique vertex IDs using a Flajolet-Martin Sketch.
 * 
 * @author Robert Waury
 * 
 */
public class FM64CounterWritable implements Writable {

	  public final static int NUM_BUCKETS = 32;
	  private final static double MAGIC_CONSTANT = 0.77351;
	  private long[] buckets;

	  /**
	   * Create a zero-bucket FM-Sketch. This is needed because Giraph requires a
	   * no-argument constructor.
	   */
	  public FM64CounterWritable() {
	    this.buckets = new long[NUM_BUCKETS];
	  }

	  /**
	   * Create a copy of the FM-Sketch by copying the internal integer array.
	   */
	  public FM64CounterWritable copy() {
	    FM64CounterWritable result = new FM64CounterWritable();
	    result.buckets = Arrays.copyOf(this.buckets, this.buckets.length);
	    return result;
	  }

	  /**
	   * Count the passed in node id.
	   * 
	   * @param n
	   */
	  public void addNode(long n) {
		for(int i = 0; i < buckets.length; i++) {
		    long hash = hash(n, i);
		    buckets[i] |= (1 << Long.numberOfTrailingZeros(hash));
		}
	  }

	  /**
	   * Return the estimate for the number of unique ids.
	   */
	  public long getCount() {
		int S = 0;
		int R = 0;
		long bucket = 0;
		long[] sorted = new long[buckets.length];
		for (int i = 0; i < buckets.length; ++i) {
		  R = 0;
		  bucket = buckets[i];
		  while ((bucket & 1) == 1 && R < Long.SIZE) {
		     ++R;
		     bucket >>= 1;
		  }
		  sorted[i] = R;
		}
		Arrays.sort(sorted);
		int start = (int) (0.25*buckets.length);
		int end = (int) (0.75*buckets.length);
		int size = end-start;
		for(int i = start; i < end; i++) {
			S += sorted[i];
		}
		long count = (long) (Math.pow(2.0, (double)S/(double)size)/MAGIC_CONSTANT);
		return count;
	  }

	  /**
	   * Merge this FM-Sketch with the other one.
	   */
	  public void merge(FM64CounterWritable other) {
	    Preconditions.checkArgument(other instanceof FM64CounterWritable,
	        "Other is not a FM64CounterWritable.");
	    FM64CounterWritable otherB = (FM64CounterWritable) other;
	    Preconditions.checkState(this.buckets.length == otherB.buckets.length,
	        "Number of buckets does not match.");
	    for (int i = 0; i < buckets.length; ++i) {
	      buckets[i] |= otherB.buckets[i];
	    }
	  }
	  
	  /**
	   * hash function used for the FM sketch (adapted MurmurHash 2.0)
	   * 
	   * @param code value (node id) to hash
	   * @param level to create different hashes for the same value
	   * @return hash value (63 significant bits)
	   */
	  private long hash(long code, int level) {
		  final long m = 0xc6a4a7935bd1e995L;
		  final int r = 47;
	      
		  byte[] data = new byte[8];
		  for (int i = 0; i < data.length; i++) {
			  data[i] = (byte) code;
			  code >>= 8;
	      }

	      long hash = (level & 0xffffffffL) ^ (8 * m);
	      long k =  ( (long) data[0] & 0xff)        + (((long) data[1] & 0xff) << 8)
	              + (((long) data[2] & 0xff) << 16) + (((long) data[3] & 0xff) << 24)
	              + (((long) data[4] & 0xff) << 32) + (((long) data[5] & 0xff) << 40)
	              + (((long) data[6] & 0xff) << 48) + (((long) data[7] & 0xff) << 56);
	            
	       k *= m;
	       k ^= k >>> r;
	       k *= m;
	            
	       hash ^= k;
	       hash *= m;
	       hash *= m;
	       hash ^= hash >>> r;
	       hash *= m;
	       hash ^= hash >>> r;

	       return hash >= 0 ? hash : -(hash + 1);
	  }

	  /**
	   * Return the number of buckets.
	   */
	  public int getNumBuckets() {
	    return buckets.length;
	  }

	  @Override
	  public void write(DataOutput out) throws IOException {
	    out.writeInt(buckets.length);
	    for (long b : buckets) {
	      out.writeLong(b);
	    }
	  }

	  @Override
	  public void readFields(DataInput in) throws IOException {
	    int numBuckets = in.readInt();
	    buckets = new long[numBuckets];
	    for (int i = 0; i < buckets.length; ++i) {
	      buckets[i] = in.readLong();
	    }
	  }

}
