
source ./run_cfg.sh

LOG="results/dataScaleResults.log"

WORKLOAD_CLASS="org.apache.giraph.examples.closeness.FMClosenessVertex"


echo "+++ MAKE SURE THAT THE CLUSTER HAS THE RIGHT SIZE!!"
# /share/hadoop/hadoop/bin/stop-mapred.sh 
# cpslaves-template /share/hadoop/hadoop/conf/slaves
# /share/hadoop/hadoop/bin/start-mapred.sh
# wait until all worker connected to the job tracker!! 

sleep 3

echo "DataSize;SecondsRuntime;OutputDirectory" >> $LOG

# 7 8 9 10 11
for SIZE in 9 ; do
	INPUT="hdfs:///user/robert/datasets/kron-$SIZE"
	OUTPUT=$OUTPUT"-"$SIZE
	# -c org.apache.giraph.examples.closeness.FMCombiner 
	CMD="$HADOOP jar ${BUILDS}${JAR} $CLASS $WORKLOAD_CLASS  -vif $INPUTFORMAT -of $OUTPUTFORMAT -vip $INPUT -op $OUTPUT -ca giraph.examples.closeness.nodeSplit=\\t -ca giraph.examples.closeness.edgeSplit=\\t -w 207"
	echo "Starting $CMD"
	startTS=`date +%s`
	$CMD
	endTS=`date +%s`
	RUNT=`expr $endTS - $startTS`
	echo "$SIZE;$RUNT;$OUTPUT" >> $LOG
done
