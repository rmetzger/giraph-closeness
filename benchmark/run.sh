source ./run_cfg.sh



WORKLOAD_CLASS="org.apache.giraph.examples.closeness.BitfieldClosenessVertex"

CMD="$HADOOP jar ${BUILDS}${JAR} $CLASS $WORKLOAD_CLASS -vif $INPUTFORMAT -of $OUTPUTFORMAT -vip $INPUT -op $OUTPUT -w 25"
echo "Starting $CMD"

$CMD
