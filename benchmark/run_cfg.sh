
if [ -f "metzger_local.sh" ]; then
	source ./metzger_local.sh
fi

if [ -z "$HADOOP" ]; then
	HADOOP="./hadoop/bin/hadoop"
fi

if [ -z "$BUILDS" ]; then
	BUILDS="/home/robert/workspace/giraph-closeness/giraph-examples/target/"
fi

function rnd() {
	tr -dc "[:alpha:]" < /dev/urandom | head -c 8
}

if [ -z "$INPUT" ]; then
        INPUT="hdfs:///user/robert/as2.txt"
fi


JAR="giraph-examples-1.1.0-SNAPSHOT-for-hadoop-1.0.2-jar-with-dependencies.jar"
CLASS="org.apache.giraph.GiraphRunner"

#INPUT="hdfs:///user/robert/as2.txt"
OUTPUT="hdfs:///user/robert/giraph/"`rnd`

INPUTFORMAT="org.apache.giraph.examples.closeness.ClosenessVertexInputFormat"
OUTPUTFORMAT="org.apache.giraph.examples.closeness.ClosenessVertexOutputFormat"
