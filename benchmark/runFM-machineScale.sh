
source ./run_cfg.sh

LOG="results/machineScaleResults.log"

WORKLOAD_CLASS="org.apache.giraph.examples.closeness.FMClosenessVertex"

echo "Cluster_size;SecondsRuntime;OutputDirectory" >> $LOG

for CLUSTER_SIZE in 8 12 16 20 26 ; do
	# create a mapred-site.xml

	# stop mapred
	/share/hadoop/hadoop/bin/stop-mapred.sh
	sleep 10

	head -n $CLUSTER_SIZE slaves-template > slaves

	# copy them
	cp slaves /share/hadoop/hadoop/conf/slaves

	#copy our nicely crafted config
	# cp mapred-site.xml-template /share/hadoop/hadoop/conf/mapred-site.xml

	# bring it up again
	/share/hadoop/hadoop/bin/start-mapred.sh
	WCOUNT=`echo "$CLUSTER_SIZE*8 -1 " | bc`
	INPUT="hdfs:///user/robert/datasets/kron-11" # 2 Billion Edges
	OUTPUT=$OUTPUT"-"$CLUSTER_SIZE
	CMD="$HADOOP jar ${BUILDS}${JAR} $CLASS $WORKLOAD_CLASS -c org.apache.giraph.examples.closeness.FMCombiner  -vif $INPUTFORMAT -of $OUTPUTFORMAT -vip $INPUT -op $OUTPUT -ca giraph.examples.closeness.nodeSplit=\\t -ca giraph.examples.closeness.edgeSplit=\\t -w $WCOUNT"
	echo "Starting $CMD"
	startTS=`date +%s`
	echo "-- SLEEPING 15 seconds for the cluster to get ready"
	sleep 15
	$CMD
	endTS=`date +%s`
	RUNT=`expr $endTS - $startTS`
	echo "Seems like this guy took $RUNT seconds to complete"
	echo "$CLUSTER_SIZE;$RUNT;$OUTPUT" >> $LOG
done
