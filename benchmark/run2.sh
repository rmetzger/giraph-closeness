
source ./run_cfg.sh

WORKLOAD_CLASS="org.apache.giraph.examples.closeness.FMClosenessVertex"


CMD="$HADOOP jar ${BUILDS}${JAR} $CLASS $WORKLOAD_CLASS -c org.apache.giraph.examples.closeness.FMCombiner  -vif $INPUTFORMAT -of $OUTPUTFORMAT -vip $INPUT -op $OUTPUT -ca giraph.examples.closeness.nodeSplit=\\t -ca giraph.examples.closeness.edgeSplit=\\t -w 207"
echo "Starting $CMD"

$CMD
