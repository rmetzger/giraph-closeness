
source ./run_cfg.sh

LOG="results/otherDatasetsResults.log"

WORKLOAD_CLASS="org.apache.giraph.examples.closeness.FMClosenessVertex"


echo "+++ MAKE SURE THAT THE CLUSTER HAS THE RIGHT SIZE!!"
# /share/hadoop/hadoop/bin/stop-mapred.sh 
# cpslaves-template /share/hadoop/hadoop/conf/slaves
# /share/hadoop/hadoop/bin/start-mapred.sh
# wait until all worker connected to the job tracker!! 

sleep 3

echo "DataSize;SecondsRuntime;OutputDirectory" >> $LOG

for FILE in webbase-2001 twitter-2010  ; do
	INPUT="hdfs:///user/robert/datasets/$FILE"
	OUTPUT=$OUTPUT"-"$FILE
	# -c org.apache.giraph.examples.closeness.FMCombiner 
	CMD="$HADOOP jar ${BUILDS}${JAR} $CLASS $WORKLOAD_CLASS  -c org.apache.giraph.examples.closeness.FMCombiner  -vif $INPUTFORMAT -of $OUTPUTFORMAT -vip $INPUT -op $OUTPUT -ca giraph.useOutOfCoreGraph=true -ca giraph.useOutOfCoreMessages=true -ca giraph.examples.closeness.nodeSplit=space -ca giraph.examples.closeness.edgeSplit=space -w 207"
	echo "Starting $CMD"
	startTS=`date +%s`
	$CMD
	endTS=`date +%s`
	RUNT=`expr $endTS - $startTS`
	echo "$SIZE;$RUNT;$OUTPUT" >> $LOG
done
