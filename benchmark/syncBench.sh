
# download .log files

scp rmetzger@cloud-11.dima.tu-berlin.de:/home/hadoop/robert/benchmarks/results/* results/


#upload scripts
scp *.sh rmetzger@cloud-11.dima.tu-berlin.de:/home/hadoop/robert/benchmarks/

scp *-template rmetzger@cloud-11.dima.tu-berlin.de:/home/hadoop/robert/benchmarks/
