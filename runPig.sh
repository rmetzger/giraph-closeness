PIG_BIN="/media/Store/data/giraph/pig-0.11.1/bin/pig"

export PIG_CLASSPATH=$PIG_CLASSPATH:/home/robert/Studium/TUBerlin/Semester2/AIM3/project/giraph-2/pig/OutputFormat/output.jar
#/home/robert/Studium/TUBerlin/Semester2/AIM3/project/giraph-2/
read -r -d '' PIG_SCRIPT <<'EOF'
REGISTER /home/robert/Studium/TUBerlin/Semester2/AIM3/project/giraph-2/pig/OutputFormat/output.jar;

indata = LOAD '/media/Store/data/giraph/inputdata/graph500/factor_25' USING PigStorage('\t') AS (from:int, to:int);

grouped = GROUP indata BY from;
STORE grouped INTO '/media/Store/data/giraph/inputdata/graph500/factor_25.grouped.asciigood' USING de.tuberlin.dima.aim3.closeness.pig.GiraphInputStoreFunc();


EOF



echo $PIG_SCRIPT | $PIG_BIN -x local