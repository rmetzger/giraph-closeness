PIG_BIN="/media/Store/data/giraph/pig-0.11.1/bin/pig"

export PIG_CLASSPATH=$PIG_CLASSPATH:/home/robert/Studium/TUBerlin/Semester2/AIM3/project/giraph-2/pig/OutputFormat/output.jar
#/home/robert/Studium/TUBerlin/Semester2/AIM3/project/giraph-2/
read -r -d '' PIG_SCRIPT <<'EOF'
REGISTER /home/robert/Studium/TUBerlin/Semester2/AIM3/project/giraph-2/pig/OutputFormat/output.jar;

indata = LOAD '/home/robert/output/sDsVxUwl/part-m-00001' USING PigStorage('\t') AS (id:int, component:int, remainder: chararray);

filtered = FILTER indata BY component == 0;
STORE filtered INTO '/media/Store/data/giraph/inputdata/enron2_oneComponent' USING PigStorage('\t') ;


EOF

# de.tuberlin.dima.aim3.closeness.pig.GiraphInputStoreFunc()

echo $PIG_SCRIPT | $PIG_BIN -x local