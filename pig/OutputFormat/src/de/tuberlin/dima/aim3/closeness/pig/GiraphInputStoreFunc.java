package de.tuberlin.dima.aim3.closeness.pig;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.pig.StoreFunc;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;

/**
 * 
 * @author Robert Metzger <metrobert@gmail.com>
 *
 */
public class GiraphInputStoreFunc extends StoreFunc{

	private static final char TO_FROM_SEPERATOR = '\t';
	private static final char TO_SEPERATOR = '\t';
	
	private static final int BUFFER_SIZE = 1024 * 4; // 4k seems to be what 
	// everybody is using!
	
	protected RecordWriter writer = null;
	StringBuffer buf = new StringBuffer();
	
	@SuppressWarnings("rawtypes")
	@Override
	public OutputFormat getOutputFormat() throws IOException {
		return new TextOutputFormat<WritableComparable, Text>();     
	}

	@Override
	public void prepareToWrite(RecordWriter writer) throws IOException {
		this.writer = writer;
	}

	/**
	 * (0,{(0,570841),(0,19622296),(0,2122674),(0,9313240),(0,27841808),(0,6326905),(0,22340155),(0,6321240),(0,864253)})
(22,{(22,4780514)})
	 * (tuple)
	 * {bag}
	 */
	@Override
	public void putNext(Tuple tup) throws IOException {
		Integer from = getIntFromTuple(0, tup);
		buf.append(from);
		buf.append(TO_FROM_SEPERATOR);
		Object toBagRaw = tup.get(1);
		if(DataType.BAG == DataType.findType(toBagRaw)) {
			Iterator<Tuple> tupleIter = ((DataBag)toBagRaw).iterator();
			while(tupleIter.hasNext()) {
				Tuple t = tupleIter.next();
				buf.append(getIntFromTuple(1, t));
				buf.append(TO_SEPERATOR);
			}
		} else {
			throw new RuntimeException("Expected "+toBagRaw+" to be a bag!");
		}
		Text text = new Text(buf.toString());
        try {
            writer.write(null, text);
            buf.setLength(0);
        } catch (InterruptedException e) {
            throw new IOException(e);
        }
	}

	private final static int getIntFromTuple(int idx, Tuple t) throws ExecException {
		Object fromRaw = t.get(idx);
		byte type = DataType.findType(fromRaw);
		if( DataType.INTEGER == type ) {
			return ((Integer)fromRaw);
		} else {
			throw new RuntimeException("Expected "+fromRaw+" to be an integer!");
		}
	}
	
	
	@Override
	public void setStoreLocation(String location, Job job) throws IOException {
	   job.getConfiguration().set("mapred.textoutputformat.separator", "");
       FileOutputFormat.setOutputPath(job, new Path(location));
	}
}
