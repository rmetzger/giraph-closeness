
if [ -f "metzger_local.sh" ]; then
	source ./metzger_local.sh
fi

if [ -z "$HADOOP" ]; then
	HADOOP="./hadoop/bin/hadoop"
fi

if [ -z "$BUILDS" ]; then
	BUILDS="/home/robert/workspace/giraph-closeness/giraph-examples/target/"
fi

if [ -z "$INPUT" ]; then
        INPUT="file:///home/robert/enron2.txt"
fi


function rnd() {
	tr -dc "[:alpha:]" < /dev/urandom | head -c 8
}

JAR="giraph-examples-1.1.0-SNAPSHOT-for-hadoop-1.0.2-jar-with-dependencies.jar"
CLASS="org.apache.giraph.GiraphRunner"

OUTPUT="file:///home/robert/output/"`rnd`

INPUTFORMAT="org.apache.giraph.examples.closeness.ConnectedComponentsInputFormat"
OUTPUTFORMAT="org.apache.giraph.examples.closeness.ConnectedComponentsOutputFormat"

WORKLOAD_CLASS="org.apache.giraph.examples.closeness.ConnectedComponentsComputation"

CMD="$HADOOP jar ${BUILDS}${JAR} $CLASS $WORKLOAD_CLASS -vif $INPUTFORMAT -of $OUTPUTFORMAT -vip $INPUT -op $OUTPUT -w 1"
echo "Starting $CMD"

$CMD
