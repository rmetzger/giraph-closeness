
if [ -f "metzger_local.sh" ]; then
	source ./metzger_local.sh
fi

if [ -z "$HADOOP" ]; then
	HADOOP="./hadoop/bin/hadoop"
fi

if [ -z "$BUILDS" ]; then
	BUILDS="/home/robert/workspace/giraph-closeness/giraph-examples/target/"
fi

if [ -z "$INPUT" ]; then
        INPUT="file:///home/robert/as2.txt"
fi


function rnd() {
	tr -dc "[:alpha:]" < /dev/urandom | head -c 8
}

JAR="giraph-examples-1.1.0-SNAPSHOT-for-hadoop-1.0.2-jar-with-dependencies.jar"
CLASS="org.apache.giraph.GiraphRunner"

OUTPUT="file:///home/robert/output/"`rnd`

INPUTFORMAT="org.apache.giraph.examples.closeness.ClosenessVertexInputFormat"
OUTPUTFORMAT="org.apache.giraph.examples.closeness.ClosenessVertexOutputFormat"
