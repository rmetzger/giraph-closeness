
source ./run_cfg.sh

OUTPUTFORMAT="org.apache.giraph.examples.closeness.ClosenessVertex64OutputFormat"

WORKLOAD_CLASS="org.apache.giraph.examples.closeness.HLLClosenessVertex"

CMD="$HADOOP jar ${BUILDS}${JAR} $CLASS $WORKLOAD_CLASS  -vif $INPUTFORMAT -of $OUTPUTFORMAT -vip $INPUT -op $OUTPUT -c org.apache.giraph.examples.closeness.HLLCombiner -w 1"
echo "Starting $CMD"

$CMD