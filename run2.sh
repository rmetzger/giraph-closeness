
source ./run_cfg.sh

WORKLOAD_CLASS="org.apache.giraph.examples.closeness.FMClosenessVertex"

# waury: -Dgiraph.zkManagerDirectory='/tmp/giraph/_bsp/_defaultZkManagerDir'

CMD="$HADOOP jar ${BUILDS}${JAR} $CLASS $WORKLOAD_CLASS  -vif $INPUTFORMAT -of $OUTPUTFORMAT -vip $INPUT -op $OUTPUT -w 1 -ca giraph.examples.closeness.nodeSplit=\t -ca giraph.examples.closeness.edgeSplit=\t"
echo "Starting $CMD"

$CMD